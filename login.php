<?php
  session_start();
  if(isset($_POST['logar'])){
    $Email = $_POST['email'];
    $Senha = $_POST['senha'];

    // Se o arquivo json do usuário existir, verificar se a senha está correta
  if (file_exists("usuarios/$Email.json")) {
    $jsonString = file_get_contents("usuarios/$Email.json");
    $jsonData = json_decode($jsonString, true);
    if (in_array($Senha, $jsonData)) {
        $_SESSION['email'] = $Email;
        header("location: site.php"); die('');
    } else {
      print "<section id='red'>Senha incorreta.</section>";
    }
  } else {
    print "<section id='red'>Email incorreto.</section>";

  }

  }
  ?>
<html>
<head>
    <title>PHP Test</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
  <section>

  <form action="login.php" method="post">
    <label>Email: </label><input type="email" name="email" placeholder="email@email.com"> <br>
    <label>Senha: </label><input type="password" name="senha" placeholder="senha"> <br>
    <input type="submit" value="Logar" name="logar">
  </form>
  </section>

  <section>
    <form action="index.php" method="post">
      <input type="submit" value="Voltar">
    </form>
  </section>
</body>
</html>
