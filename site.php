<?php
  session_start();
  // Delogar se o usuário clicar no botão
  if (isset($_POST['sair'])){
    unset($_SESSION['email']);
    session_destroy();
    header("Location: index.php");
  }
  // Recuperar os dados do arquivo json referentes à sessão do usuário
  $jsonString = file_get_contents("usuarios/{$_SESSION['email']}.json");
  $jsonData = json_decode($jsonString, true);
?>

<html>
<head>
  <link rel="stylesheet" href="style.css">

</head>

<body>

<h1>Parabéns! Você logou</h1>
<b>Você é o usuário <b><?= $_SESSION['email'] ?></b></b>
  <p> Seu nome é <?php echo $jsonData['Nome'];;?></p>
  <p> Sua fruta favorita é <?php echo $jsonData['Fruta'];?>;</p>
<form action="site.php" method="post">
  <input type="submit" value="Deslogar" name="sair">
</form>

  <br>
<a href="index.php">Voltar</a>

<section id="red2"><a href="https://gitlab.com/maiaaf_public/lp-ii-login-e-cadastro-com-php-e-json">Repositório no Gitlab</a></section>
</body>


</html>

