<?php
  // Arthur - 1813
  // Inicia a sessão que irá armazenar o login
  // Inclusive, eu usei sessão porque é o meu tema do seminário
  
  session_start();
  if (isset($_POST['logout'])){
    unset($_SESSION['email']);
    session_destroy();
    header("Location: index.php");
  }
  if (isset($_SESSION["email"])){
    print "<section>logado como {$_SESSION['email']}</section>";
  }
?>
<html>
<head>
  <title>PHP Test</title>
  <link rel="stylesheet" href="style.css">
</head>
  <body>
    <section>
      <h1>Você deseja logar ou se cadastrar?</h1>
      <?php // Imprime opções diferentes dependendo se o usuário está logado ou não
      if (isset($_SESSION["email"])) {
          print "<br><br><form action='site.php' method='post'>
          <input type='submit' value='Ver perfil'>
          </form>";
          print "<br><br><form action='index.php' method='post'>
          <input type='submit' value='Sair' name='logout'>
          </form>";
        }
        else {
        print "<form action='login.php' method='post'>
          <input type='submit' value='Login'>
        </form>";
        print "<form action='cadastro.php' method='post'>
      <input type='submit' value='Cadastrar'>";
      }
      ?>

    </section>
  </body>
</html>
